%{ 
    #include "y.tab.h"   /*todos os defines dos tokens utilizados (nomes e atributos)*/
    /* variável para arazenar o número de linhas analisadas */
    int quant_linhas =  1;
    #include<stdio.h>
    #include <stdlib.h>
    #include <string.h>
%} 


/* Definições e expressões Regulares */ 

espacamento 			[ \t]
espacamentos            {espacamento}+
digito			        [0-9]
letra                   [A-Za-z]
numero                  {digito}+(\.{digito}+)?(E[\+\-]?{digito}+)?
identificador 			{letra}({letra}|{digito})*
operador_unario 		{identificador}\-


%% 

{espacamentos}          {/* Nada a se fazer, apenas ignora */}
\n                      {quant_linhas++;}
"/*"[^\n]*"*/"		     /* comentarios sao ignorados */
begin                   {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return LBEGIN;}
boolean                 {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return BOOLEAN;}
char                    {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return CHAR;}
do                      {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return DO;}
else                    {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return ELSE;}
end                     {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return END;}
false                   {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return FALSE;}
endif                   {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return ENDIF;}
endwhile                {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return ENDWHILE;}
exit                    {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return EXIT;}
if                      {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return IF;}
integer                 {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return INTEGER;}
procedure               {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return PROCEDURE;}
program                 {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return PROGRAM;}
reference               {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return REFERENCE;}
repeat                  {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return REPEAT;}
read                    {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return READ;}
return                  {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return RETURN;}
then                    {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return THEN;}
true                    {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return TRUE;}
type                    {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return TYPE;}
until                   {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return UNTIL;}
value                   {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return VALUE;}
write                   {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return WRITE;}
while                   {printf ("Linha [%i] com token [nome] - [%s]\n", quant_linhas, yytext); return WHILE;}
"+"                     {printf ("Linha [%i] com token [nome,atributo] - [%s,%i]\n", quant_linhas, yytext, SUM); return SUM;}
"-"                     {printf ("Linha [%i] com token [nome,atributo] - [%s,%i]\n", quant_linhas, yytext, SUB); return SUB;}
"*"                     {printf ("Linha [%i] com token [nome,atributo] - [%s,%i]\n", quant_linhas, yytext, MUL); return MUL;}
"/"                     {printf ("Linha [%i] com token [nome,atributo] - [%s,%i]\n", quant_linhas, yytext, DIV); return DIV;}
"**"                    {printf ("Linha [%i] com token [nome,atributo] - [%s,%i]\n", quant_linhas, yytext, POW); return POW;}
"<"                     {printf ("Linha [%i] com token [nome,atributo] - [%s,%i]\n", quant_linhas, yytext, LT); return LT;}
">"                     {printf ("Linha [%i] com token [nome,atributo] - [%s,%i]\n", quant_linhas, yytext, GT); return GT;}
"<="                    {printf ("Linha [%i] com token [nome,atributo] - [%s,%i]\n", quant_linhas, yytext, LE); return LE;}
">="                    {printf ("Linha [%i] com token [nome,atributo] - [%s,%i]\n", quant_linhas, yytext, GE); return GE;}
"="                     {printf ("Linha [%i] com token [nome,atributo] - [%s,%i]\n", quant_linhas, yytext, EQ); return EQ;}
"not="                  {printf ("Linha [%i] com token [nome,atributo] - [%s,%i]\n", quant_linhas, yytext, NE); return NE;}
"&"                     {printf ("Linha [%i] com token [nome,atributo] - [%s,%i]\n", quant_linhas, yytext, AND); return AND;}
"|"                     {printf ("Linha [%i] com token [nome,atributo] - [%s,%i]\n", quant_linhas, yytext, OR); return OR;}
"not"                   {printf ("Linha [%i] com token [nome,atributo] - [%s,%i]\n", quant_linhas, yytext, NOT); return NOT;}
{operador_unario}		{printf ("Linha [%i] com token [nome,atributo] - [%s,%s]\n", quant_linhas, yytext, ""); return  UNARY;}
{identificador} 	    {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, ID, yytext); return ID;}
{numero}	 	        {printf ("Linha [%i] com token [nome,atributo] - [%i,%s]\n", quant_linhas, NUM, yytext); return NUM;}
"("			            {printf ("Linha [%i] com token [nome,atributo] - [%s,%i]\n", quant_linhas, yytext, OPENP); return OPENP;}  
")"		                {printf ("Linha [%i] com token [nome,atributo] - [%s,%i]\n", quant_linhas, yytext, CLOSEP); return CLOSEP;} 	
";"		                {printf ("Linha [%i] com token [nome,atributo] - [%s,%i]\n", quant_linhas, yytext, ENDLINE); return ENDLINE;}
":=" 			        {printf ("Linha [%i] com token [nome,atributo] - [%s,%i]\n", quant_linhas, yytext, ASSIGN); return ASSIGN;}  
":"			            {printf ("Linha [%i] com token [nome,atributo] - [%s,%i]\n", quant_linhas, yytext, DEFTYPE); return DEFTYPE;}
","			            {printf ("Linha [%i] com token [nome,atributo] - [%s,%i]\n", quant_linhas, yytext, SEPAR); return SEPAR;}     	
. 			            {printf("Linha [%i] com algum caractere invalido para a linguagem! [%s]\n",quant_linhas,yytext); return LEXERROR;}

%% 

int yywrap(void){
    return 1;
}


