program
	integer : raio, base, altura, areaCirc, areaTri;
    boolean : ehmaior;
    /* testando comentarios para o programa */
begin
	raio := 100;
    base := 50;
    altura := 80;
	areaCirc := raio**2 * 3.14;
    areaTri := (base * altura)/2;
	
    if (areaCirc > areaTri) then write areaCirc; ehmaior := true /* testando if-else */
    else write areaTri endif  
    
end	
