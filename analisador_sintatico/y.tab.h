/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    LBEGIN = 258,
    BOOLEAN = 259,
    CHAR = 260,
    DO = 261,
    ELSE = 262,
    END = 263,
    ENDIF = 264,
    ENDWHILE = 265,
    EXIT = 266,
    FALSE = 267,
    IF = 268,
    INTEGER = 269,
    PROCEDURE = 270,
    PROGRAM = 271,
    READ = 272,
    REFERENCE = 273,
    REPEAT = 274,
    RETURN = 275,
    THEN = 276,
    TRUE = 277,
    TYPE = 278,
    UNTIL = 279,
    VALUE = 280,
    WHILE = 281,
    WRITE = 282,
    SUM = 283,
    SUB = 284,
    MUL = 285,
    DIV = 286,
    POW = 287,
    UNARY = 288,
    LT = 289,
    GT = 290,
    LE = 291,
    GE = 292,
    EQ = 293,
    NE = 294,
    OR = 295,
    AND = 296,
    NOT = 297,
    ID = 298,
    NUM = 299,
    OPENP = 300,
    CLOSEP = 301,
    ENDLINE = 302,
    ASSIGN = 303,
    DEFTYPE = 304,
    SEPAR = 305,
    LEXERROR = 306
  };
#endif
/* Tokens.  */
#define LBEGIN 258
#define BOOLEAN 259
#define CHAR 260
#define DO 261
#define ELSE 262
#define END 263
#define ENDIF 264
#define ENDWHILE 265
#define EXIT 266
#define FALSE 267
#define IF 268
#define INTEGER 269
#define PROCEDURE 270
#define PROGRAM 271
#define READ 272
#define REFERENCE 273
#define REPEAT 274
#define RETURN 275
#define THEN 276
#define TRUE 277
#define TYPE 278
#define UNTIL 279
#define VALUE 280
#define WHILE 281
#define WRITE 282
#define SUM 283
#define SUB 284
#define MUL 285
#define DIV 286
#define POW 287
#define UNARY 288
#define LT 289
#define GT 290
#define LE 291
#define GE 292
#define EQ 293
#define NE 294
#define OR 295
#define AND 296
#define NOT 297
#define ID 298
#define NUM 299
#define OPENP 300
#define CLOSEP 301
#define ENDLINE 302
#define ASSIGN 303
#define DEFTYPE 304
#define SEPAR 305
#define LEXERROR 306

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef int YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */
