program 
	integer : x := 2, 2z; /*Identificador invalido */
begin
	while (x < 1024) 
        do x := x**2; 
    endwhile
    
    w := x; /* variavel nao declarada */
    repet /* comando tratado como identificador */
        w := w/2;
    until (w > 2) 
	
end
